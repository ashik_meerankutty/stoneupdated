$('.main_slider').owlCarousel({
    loop:true,
    margin:0,
    nav:false,
    dots:true,
    dotsEach:true,
    autoplay:true,
    items:1,
})

$(document).ready(function(){
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    dots:true,
    dotsEach:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:3,
            loop:false
        }
    }
})
});

$(window).scroll(function () {
    if ($(document).scrollTop() > 150 || $( window ).width()<1000) {
        $('nav').addClass('navbar-fixed-top');
        $('nav').addClass('shrink');
        $('body').addClass('top-pad');
    } else {
        $('nav').removeClass('shrink');
        $('nav').removeClass('navbar-fixed-top');
        $('body').removeClass('top-pad');
    }
});

if($( window ).width()<1000)
{
  $('nav').addClass('navbar-fixed-top');
  $('nav').addClass('shrink');
  $('body').addClass('top-pad');
}
$(function() {
	$('.match-1').matchHeight();
});
$(function() {
	$('.match-2').matchHeight();
});
$(function() {
	$('.match').matchHeight();
});
$(function() {
	$('.match-about').matchHeight();
});
$(function() {
	$('.m8').matchHeight();
});
$(function() {
	$('.m9').matchHeight();
});

$(function () {
    var Page = (function () {
        var $navArrows = $('#nav-arrows'),
                $nav = $('#nav-dots > span'),
                slitslider = $('#slider').slitslider({
            onBeforeChange: function (slide, pos) {

                $nav.removeClass('nav-dot-current');
                $nav.eq(pos).addClass('nav-dot-current');
            }
        }),
        init = function () {

            initEvents();
        },
        initEvents = function () {

            $navArrows.children(':last').on('click', function () {
                slitslider.next();
                return false;
            });

            $navArrows.children(':first').on('click', function () {
                slitslider.previous();
                return false;
            });

            $nav.each(function (i) {
                $(this).on('click', function (event) {
                    var $dot = $(this);
                    if (!slitslider.isActive()) {
                        $nav.removeClass('nav-dot-current');
                        $dot.addClass('nav-dot-current');
                    }
                    slitslider.jump(i + 1);
                    return false;
                });
            });

            var tid = setTimeout(mycode, 3000);
            function mycode() {
              slitslider.next();
              tid = setTimeout(mycode, 3000);
            }

        };
        return {init: init};
    })();
    Page.init();
});

$('#item1').click(function (e) {
  e.preventDefault();
  $('#item2-tab').removeClass('active');
  $('#item3-tab').removeClass('active');
  $('#item4-tab').removeClass('active');
  $('#item1-tab').addClass('active');
})
$('#item2').click(function (e) {
  e.preventDefault();
  $('#item1-tab').removeClass('active');
  $('#item3-tab').removeClass('active');
  $('#item4-tab').removeClass('active');
  $('#item2-tab').addClass('active');
})
$('#item3').click(function (e) {
  e.preventDefault();
  $('#item1-tab').removeClass('active');
  $('#item2-tab').removeClass('active');
  $('#item4-tab').removeClass('active');
  $('#item3-tab').addClass('active');
})
$('#item4').click(function (e) {
  e.preventDefault();
  $('#item1-tab').removeClass('active');
  $('#item2-tab').removeClass('active');
  $('#item3-tab').removeClass('active');
  $('#item4-tab').addClass('active');
})

$('#submit').click(function () {

			    /*$.ajax({
			        type: 'POST',
			        url: 'savebill.php',
			        data: null,
			        beforeSend: function (html) {
			        },
			        success: function (html) {
			            saveallitems();
			        }
			    });*/

			    contact_form = $("#contact_form");
			    console.log($("#contact_form").serialize());
			    $.ajax({
			    type: 'POST',
			    url: '/submit/',
			    data: contact_form.serialize(),
			    dataType: 'json',
			    success: function(data) {
		            $('#result').text(data.result);
			    },
			    error: function() {
			    	console.log("aa");
				}
				});
			})
