from django.forms import ModelForm
from .models import Message


class contactForm(ModelForm):
	class Meta:
		model = Message
		fields = ['firstName','email','phoneNumber','message']
