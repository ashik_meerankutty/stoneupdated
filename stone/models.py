from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Products(models.Model):
    title = models.CharField(max_length = 200)
    description = models.TextField()
    image = models.FileField(null = False, blank = True)
    productType = models.CharField(max_length = 200)
    featured = models.BooleanField(default = False)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.title

class Message(models.Model):
    firstName = models.CharField(max_length = 200)
    email = models.EmailField(max_length = 200)
    phoneNumber = models.CharField(max_length = 200)
    message = models.TextField()

    def __str__(self):
        return self.firstName
