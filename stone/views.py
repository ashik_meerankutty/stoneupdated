from django.shortcuts import render
from .models import Products,Message
from django.http import Http404,JsonResponse,HttpResponse
from .forms import contactForm
from django.shortcuts import render_to_response
from django.template import RequestContext
# Create your views here.

def index(request):
    products = Products.objects.filter(featured = True).order_by('-pub_date');
    context = {'products': products}
    return render(request, 'stone/index.html',context)
def about(request):
    return render(request, 'stone/about.html')
def services(request):
    return render(request, 'stone/services.html')
def products(request):
    products = Products.objects.order_by('-pub_date')
    context = {'products': products}
    return render(request, 'stone/products.html',context)
def contact(request):
    return render(request, 'stone/contact.html')

def MessageView(request):
    if request.method == 'POST':
        firstName = request.POST.get('fname','')
        email = request.POST.get('email','')
        phoneNumber = request.POST.get('pno','')
        message = request.POST.get('message','')
        if firstName and email and phoneNumber and message:
            message_obj = Message(firstName = firstName,email=email,phoneNumber=phoneNumber,message=message)
            message_obj.save()
            data = {
                    'result': 'Message Sent. We will contact you shortly.',
                    'message': 'Message Sent.'
                }
        else:
            data = {
                    'result': 'All fields are required.',
                    'message': 'Message Sent.'
                }
        return JsonResponse(data)
    return HttpResponse('<h1>Page was not found</h1>')
def handler404(request):
    response = render_to_response('404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response


def handler500(request):
    response = render_to_response('505.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 500
    return response
